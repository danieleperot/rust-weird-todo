CREATE TABLE IF NOT EXISTS todos
(
    id          VARCHAR(30) PRIMARY KEY NOT NULL,
    description TEXT                NOT NULL
);