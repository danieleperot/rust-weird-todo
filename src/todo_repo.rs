use std::any::Any;
use async_trait::async_trait;

#[async_trait]
pub trait TodoRepo {
    fn as_any(&self) -> &dyn Any;
    async fn load(self: &mut Self, name: &String) -> Option<String>;
    async fn save(self: &mut Self, name: &String, content: String) -> Result<(), ()>;
}

pub type TodoRepoBox = Box<dyn TodoRepo>;
