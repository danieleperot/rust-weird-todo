use std::any::Any;
use std::collections::HashMap;
use async_trait::async_trait;
use crate::todo_repo::TodoRepo;

#[derive(Default, Debug)]
pub struct MemoryTodoRepo {
    memory: HashMap<String, String>
}

impl MemoryTodoRepo {
    pub fn new() -> Self {
        MemoryTodoRepo { memory: HashMap::new() }
    }
}

#[async_trait]
impl TodoRepo for MemoryTodoRepo {
    fn as_any(&self) -> &dyn Any {
        self
    }

    async fn load(self: &mut Self, name: &String) -> Option<String> {
        let result = self.memory.get(name.as_str())?;

        Some(result.clone())
    }

    async fn save(self: &mut Self, name: &String, content: String) -> Result<(), ()> {
        self.memory.insert(name.clone(), content);
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn starts_empty() {
        let repo = MemoryTodoRepo::new();

        assert!(repo.memory.is_empty());
    }

    #[tokio::test]
    pub async fn returns_none_when_trying_to_get_unknown_todo() {
        let mut repo = MemoryTodoRepo::new();
        let key = "::key::".to_string();

        assert_eq!(None, repo.load(&key).await)
    }

    #[tokio::test]
    pub async fn stores_todo() {
        let mut repo = MemoryTodoRepo::new();
        let key = "::key::".to_string();
        repo.save(&key, String::from("::text::")).await.expect("Could not store text");

        assert_eq!(Some(&"::text::".to_string()), repo.memory.get(&key))
    }

    #[tokio::test]
    pub async fn retrieves_stored_todo() {
        let mut repo = MemoryTodoRepo::new();
        let key = "::key::".to_string();
        repo.memory.insert(key.clone(), String::from("::text::"));

        assert_eq!(Some("::text::".to_string()), repo.load(&key).await)
    }
}