use std::any::Any;
use std::fs;
use async_trait::async_trait;
use crate::todo_repo::TodoRepo;

pub struct FilesystemTodoRepo {
    base_dir: String
}

impl FilesystemTodoRepo {
    pub fn new() -> Self {
        FilesystemTodoRepo { base_dir: "storage/todos".to_string() }
    }

    fn get_path(&self, name: &String) -> String {
        format!("{}/{name}.txt", &self.base_dir)
    }
}

#[async_trait]
impl TodoRepo for FilesystemTodoRepo {
    fn as_any(&self) -> &dyn Any {
        self
    }

    async fn load(self: &mut Self, name: &String) -> Option<String> {
        match fs::read_to_string(self.get_path(name)) {
            Ok(result) => Some(result),
            Err(_) => None
        }
    }

    async fn save(self: &mut Self, name: &String, content: String) -> Result<(), ()> {
        fs::create_dir_all(&self.base_dir).expect("Could not create TODO storage folder!");

        match fs::write(self.get_path(name), content) {
            Ok(_) => Ok(()),
            Err(error) => { dbg!(error); Err(()) }
        }
    }
}