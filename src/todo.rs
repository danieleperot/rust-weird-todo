use async_trait::async_trait;
use rand::{Rng, thread_rng};
use crate::todo_repo::TodoRepoBox;

pub struct TodoAssistant {
    repo: TodoRepoBox
}

impl TodoAssistant {
    pub fn new(repo: TodoRepoBox) -> Self {
        TodoAssistant { repo }
    }

    pub async fn remember(self: &mut Self, content: String) -> String {
        let id = thread_rng().gen::<u32>().to_string();
        self.repo.save(&id, content).await.unwrap();

        id
    }

    pub async fn read(self: &mut Self, id: &String) -> String {
        match self.repo.load(id).await {
            Some(s) => s.clone(),
            None => "Not found".to_string()
        }
    }
}