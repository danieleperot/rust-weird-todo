use std::any::Any;
use async_trait::async_trait;
use sqlx::{Error, Pool, Sqlite, SqlitePool};
use sqlx::pool::PoolConnection;
use crate::todo_repo::TodoRepo;

pub struct SqliteTodoRepo {
    connection: PoolConnection<Sqlite>
}

impl SqliteTodoRepo {
    pub async fn new() -> Self {
        SqliteTodoRepo {
            connection: SqlitePool::connect(&dotenv::var("DATABASE_URL").unwrap())
                .await
                .unwrap()
                .acquire()
                .await
                .unwrap()
        }
    }

    async fn create_empty_todo_if_missing(self: &mut Self, id: &String) {
        if let None = self.load(id).await {
            sqlx::query!(
            "INSERT INTO todos (id, description) VALUES (?1, \"\")",
            id
        )
                .execute(&mut self.connection)
                .await
                .unwrap()
                .last_insert_rowid();
        }
    }
}

#[async_trait]
impl TodoRepo for SqliteTodoRepo {
    fn as_any(&self) -> &dyn Any {
        self
    }

    async fn load(self: &mut Self, id: &String) -> Option<String> {
        let content = sqlx::query!("SELECT description FROM todos WHERE id = ?1 LIMIT 1", id)
            .fetch_optional(&mut self.connection)
            .await
            .unwrap();

        match content {
            None => None,
            Some(content) => Some(content.description)
        }
    }

    async fn save(self: &mut Self, id: &String, content: String) -> Result<(), ()> {
        self.create_empty_todo_if_missing(id).await;
        sqlx::query!(
            "UPDATE todos SET description = ?1 WHERE id = ?2",
            content,
            id
        )
            .execute(&mut self.connection)
            .await
            .unwrap()
            .last_insert_rowid();

        Ok(())
    }
}