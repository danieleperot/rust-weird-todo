#![allow(dead_code, unused_imports)]

use crate::database::sqlite::todo::SqliteTodoRepo;
use crate::filesystem::todo::FilesystemTodoRepo;
use crate::todo_repo::TodoRepoBox;
use crate::in_memory::todo::MemoryTodoRepo;
use crate::todo::TodoAssistant;

mod todo_repo;
mod in_memory;
mod filesystem;
mod todo;
mod database;

#[tokio::main]
async fn main () {
    // let mut todo = TodoAssistant::new(Box::new(FileFooRepo::new()));
    // let mut todo = TodoAssistant::new(Box::new(MemoryFooRepo::new()));
    let mut todo = TodoAssistant::new(Box::new(SqliteTodoRepo::new().await));

    let todo_id = todo.remember("test".to_string()).await;
    println!("Stored todo with ID {todo_id}.");

    println!("{}", todo.read(&todo_id).await);
    println!("{}", todo.read(&"hello".to_string()).await);
}
